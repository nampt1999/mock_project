create database hotel;
use hotel;

create table room(
		room_id INT PRIMARY KEY AUTO_INCREMENT,
        room_number VARCHAR(10),
        room_type VARCHAR(10) CHECK(room_type ='A' or room_type ='B' or room_type ='C'),
        price INT,
        status VARCHAR(10) CHECK(status = 'active' OR status = 'nonactive' )
);

CREATE TABLE guest(
	guest_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    guest_name VARCHAR(50),
    address VARCHAR(50),
    phone VARCHAR(50)
);

CREATE TABLE staff(
	staff_id INT PRIMARY KEY AUTO_INCREMENT,
    staff_name VARCHAR(50),
    address VARCHAR(50),
    phone VARCHAR(50),
    username VARCHAR(50),
    password VARCHAR(50),
    role VARCHAR(10) check( role='admin' or role='user' )
);
CREATE TABLE booking(
		booking_id INT PRIMARY KEY AUTO_INCREMENT,
        guest_id INT,
        room_id INT,
        staff_id INT,
        date_begin DATE,
        date_end DATE,
        CONSTRAINT fk_booking_guest FOREIGN KEY(guest_id) REFERENCES guest(guest_id),
        CONSTRAINT fk_booking_room FOREIGN KEY(room_id) REFERENCES room(room_id),
        CONSTRAINT fk_booking_staff FOREIGN KEY(staff_id) REFERENCES staff(staff_id)
);

CREATE TABLE bill(
	bill_id INT PRIMARY KEY AUTO_INCREMENT,
	booking_id INT,
    payment_date DATE,
    payment_amount INT,
    payment_type VARCHAR(10) CHECK(payment_type='tiền mặt' OR payment_type='card'),
    note VARCHAR(50),
    CONSTRAINT fk_bill_booking FOREIGN KEY(booking_id) REFERENCES booking(booking_id)
);

INSERT INTO `hotel`.`staff` (`staff_id`, `staff_name`, `address`, `phone`, `username`, `password`, `role`) VALUES ('1', 'nam', 'hà nội', '0348984167', 'nam', '123', 'admin1');
INSERT INTO `hotel`.`staff` (`staff_id`, `staff_name`, `address`, `phone`, `username`, `password`, `role`) VALUES ('2', 'hương', 'phú thọ', '034857682', 'huong', '123', 'user');

INSERT INTO `hotel`.`room` (`room_id`, `room_number`, `room_type`, `price`, `status`) VALUES ('1', '101', 'A', '300', 'active');
INSERT INTO `hotel`.`room` (`room_id`, `room_number`, `room_type`, `price`, `status`) VALUES ('2', '102', 'B', '200', 'nonactive');
INSERT INTO `hotel`.`room` (`room_id`, `room_number`, `room_type`, `price`, `status`) VALUES ('3', '103', 'C', '100', 'active');

INSERT INTO `hotel`.`guest` (`guest_id`, `guest_name`, `address`, `phone`) VALUES ('1', 'Lan Hạ', 'Thái Nguyên', '0345655168');
INSERT INTO `hotel`.`guest` (`guest_id`, `guest_name`, `address`, `phone`) VALUES ('2', 'Phùng Hà', 'Bắc Ninh', '06574555548');

INSERT INTO `hotel`.`booking` (`booking_id`, `guest_id`, `room_id`, `staff_id`, `date_begin`, `date_end`) VALUES ('1', '1', '2', '1', '2019-6-26', '2019-6-30');
INSERT INTO `hotel`.`booking` (`booking_id`, `guest_id`, `room_id`, `staff_id`, `date_begin`, `date_end`) VALUES ('2', '2', '3', '2', '2020-5-5', '2020-5-15');
INSERT INTO `hotel`.`booking` (`booking_id`, `guest_id`, `room_id`, `staff_id`, `date_begin`, `date_end`) VALUES ('3', '1', '1', '1', '2020-3-23', '2020-3-27');

INSERT INTO `hotel`.`bill` (`bill_id`, `booking_id`, `payment_date`, `payment_amount`, `payment_type`, `note`) VALUES ('1', '1', '2019-6-30', '800', 'tiền mặt', 'trả đủ');
INSERT INTO `hotel`.`bill` (`bill_id`, `booking_id`, `payment_date`, `payment_amount`, `payment_type`, `note`) VALUES ('2', '2', '2020-5-15', '700', 'card', '80%');
INSERT INTO `hotel`.`bill` (`bill_id`, `booking_id`, `payment_date`, `payment_amount`, `payment_type`, `note`) VALUES ('3', '2', '2020-5-16', '300', 'tiền mặt', 'trả đủ');




