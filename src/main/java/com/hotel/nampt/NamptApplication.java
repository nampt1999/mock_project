package com.hotel.nampt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NamptApplication {

    public static void main(String[] args) {
        SpringApplication.run(NamptApplication.class, args);
    }

}
