package com.hotel.nampt.controller;

import com.hotel.nampt.domain.response.BaseResponse;
import com.hotel.nampt.domain.response.StaffListResponse;
import com.hotel.nampt.domain.response.StaffResponse;
import com.hotel.nampt.model.Staff;
import com.hotel.nampt.service.IStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestStaffController {
    @Autowired
    private IStaffService staffService;


//    @GetMapping("/staffList")
//    public ResponseEntity<StaffListResponse> findAllProduct() {
//        StaffListResponse staffListResponse = new StaffListResponse();
//        staffListResponse.setCode(0);
//        staffListResponse.setMessage("success");
//        staffListResponse.setStaffList(staffService.findAll());
//        return ResponseEntity.ok(staffListResponse);
//    }

    @PostMapping("/staffLogin")
    public ResponseEntity<StaffResponse> checkLogin(@RequestBody Staff staff){
        return ResponseEntity.ok(staffService.checkLogin(staff));
    }
}
