package com.hotel.nampt.controller;

import com.hotel.nampt.domain.response.GuestListResponse;
import com.hotel.nampt.service.IGuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestGuestController {
    @Autowired
    private IGuestService guestService;

    @GetMapping("/guestList")
    public ResponseEntity<GuestListResponse> findAllProduct() {
        return ResponseEntity.ok(guestService.findall());
    }
}
