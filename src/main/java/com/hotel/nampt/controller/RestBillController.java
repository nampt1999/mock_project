package com.hotel.nampt.controller;

import com.hotel.nampt.domain.response.BaseResponse;
import com.hotel.nampt.domain.response.BillListResponse;
import com.hotel.nampt.domain.response.BillResponse;
import com.hotel.nampt.model.Bill;
import com.hotel.nampt.service.IBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RestBillController {
    @Autowired
    private IBillService billService;

    @GetMapping("/bill/billList")
    public ResponseEntity<BillListResponse> findAllProduct() {
        return ResponseEntity.ok(billService.findAll());
    }

//    @GetMapping("/bill/{id}")
//    public ResponseEntity<Bill> getBill(@PathVariable Integer id) {
//        return new ResponseEntity<>(billService.findById(id), HttpStatus.OK);
//    }

    @PostMapping("/bill/save")
    public ResponseEntity<BillResponse> saveOrUpdateBill(@RequestBody Bill bill) {

        return  ResponseEntity.ok(billService.save(bill));
    }

    @DeleteMapping("/bill/delete/{id}")
    public ResponseEntity<BaseResponse> deleteCompany(@PathVariable Integer id) {
        return ResponseEntity.ok(billService.delete(id));
    }

}
