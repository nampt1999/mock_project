package com.hotel.nampt.controller;

import com.hotel.nampt.domain.response.BillListResponse;
import com.hotel.nampt.domain.response.BookingListResponse;
import com.hotel.nampt.service.IBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestBookingController {
    @Autowired
    private IBookingService bookingService;

    @GetMapping("/bookingList")
    public ResponseEntity<BookingListResponse> findAllBooking() {
        return ResponseEntity.ok(bookingService.findAll());
    }

}
