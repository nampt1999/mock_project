package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.BookingListResponse;
import com.hotel.nampt.model.Booking;

import java.util.List;

public interface IBookingService {
    BookingListResponse findAll();
}
