package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.StaffResponse;
import com.hotel.nampt.model.Staff;
import com.hotel.nampt.repo.StaffRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StaffService implements IStaffService {
    @Autowired
    private StaffRepo staffRepo;

//    @Override
//    public List<Staff> findAll() {
//        return staffRepo.findAll();
//    }

//    @Override
//    public Optional<Staff> findById(Long id) {
//        return staffRepo.findById(id);
//    }

    @Override
    public StaffResponse checkLogin(Staff staff) {
        StaffResponse staffResponse = new StaffResponse();
        Staff st = staffRepo.findByUsernameAndPassword(staff.getUsername(), staff.getPassword());
        if (st != null) {
            staffResponse.setCode(100);
            staffResponse.setMessage("success !");
            staffResponse.setStaff(st);
        } else {
            staffResponse.setCode(101);
            staffResponse.setMessage("Username or Password wrong !");
            staffResponse.setStaff(null);
        }
        return staffResponse;
    }


}
