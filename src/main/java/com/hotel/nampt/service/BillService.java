package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.BaseResponse;
import com.hotel.nampt.domain.response.BillListResponse;
import com.hotel.nampt.domain.response.BillResponse;
import com.hotel.nampt.model.Bill;
import com.hotel.nampt.repo.BillRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BillService implements IBillService {

    @Autowired
    private BillRepo billRepo;

    @Override
    public BillListResponse findAll() {
        BillListResponse billListResponse = new BillListResponse();
        List<Bill> billList = billRepo.findAll();
        if (billList != null) {
            billListResponse.setCode(200);
            billListResponse.setMessage("success");
            billListResponse.setBillList(billList);
        } else {
            billListResponse.setCode(201);
            billListResponse.setMessage("null");
        }
        return billListResponse;
    }

//    @Override
//    public Bill findById(Integer id) {
//        return billRepo.findById(id).get();
//    }

    @Override
    public BillResponse save(Bill bill) {
        BillResponse billResponse = new BillResponse();
        Bill bill1 = billRepo.save(bill);
        billResponse.setCode(200);
        billResponse.setMessage("success");
        billResponse.setBill(bill1);
        return billResponse;
    }

    @Override
    public BaseResponse delete(Integer id) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            billRepo.deleteById(id);
            baseResponse.setCode(300);
            baseResponse.setMessage("delete success");
        } catch (Exception ex) {
            baseResponse.setCode(301);
            baseResponse.setMessage(ex.getMessage());
        }
        return baseResponse;
    }
}
