package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.GuestListResponse;
import com.hotel.nampt.model.Guest;

import java.util.List;

public interface IGuestService {
    GuestListResponse findall();
}
