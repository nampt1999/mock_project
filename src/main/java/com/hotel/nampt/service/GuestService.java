package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.GuestListResponse;
import com.hotel.nampt.model.Guest;
import com.hotel.nampt.repo.GuestRepo;
import com.hotel.nampt.repo.StaffRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestService implements IGuestService {

    @Autowired
    private GuestRepo guestRepo;

    @Override
    public GuestListResponse findall() {
        List<Guest>  guestList = guestRepo.findAll();
        GuestListResponse guestListResponse = new GuestListResponse();
        if(guestList != null){
            guestListResponse.setCode(200);
            guestListResponse.setMessage("success");
            guestListResponse.setGuestList(guestList);
        }else {
            guestListResponse.setCode(201);
            guestListResponse.setMessage("null");
        }
        return guestListResponse ;
    }
}
