package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.StaffResponse;
import com.hotel.nampt.model.Staff;

import java.util.List;
import java.util.Optional;

public interface IStaffService {
//    List<Staff> findAll();
//
//    Optional<Staff> findById(Long id);

    StaffResponse checkLogin(Staff staff);
}
