package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.BookingListResponse;
import com.hotel.nampt.model.Booking;
import com.hotel.nampt.repo.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService implements IBookingService {
    @Autowired
    private BookingRepo bookingRepo;

    @Override
    public BookingListResponse findAll() {
        BookingListResponse bookingListResponse = new BookingListResponse();
        List<Booking> bookingList = bookingRepo.findAll();
       if(bookingList != null){
           bookingListResponse.setCode(200);
           bookingListResponse.setMessage("success");
           bookingListResponse.setBookingList(bookingList);
       }
       return bookingListResponse;
    }
}
