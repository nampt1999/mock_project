package com.hotel.nampt.service;

import com.hotel.nampt.domain.response.BaseResponse;
import com.hotel.nampt.domain.response.BillListResponse;
import com.hotel.nampt.domain.response.BillResponse;
import com.hotel.nampt.model.Bill;

import java.util.List;
import java.util.Optional;

public interface IBillService {
    BillListResponse findAll();

//    Bill findById(Integer id);

    BillResponse save(Bill emp);

    BaseResponse delete(Integer id);



}
