package com.hotel.nampt.repo;

import com.hotel.nampt.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface BillRepo extends JpaRepository<Bill,Integer> {
    @Override
    <S extends Bill> S save(S entity);

    @Override
    Bill getOne(Integer integer);
}
