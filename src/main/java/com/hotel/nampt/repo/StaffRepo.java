package com.hotel.nampt.repo;

import com.hotel.nampt.model.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepo extends JpaRepository<Staff,Long> {
       Staff findByUsernameAndPassword(String username,String password);
}
