package com.hotel.nampt.repo;

import com.hotel.nampt.model.Guest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuestRepo extends JpaRepository<Guest,Integer> {

}
