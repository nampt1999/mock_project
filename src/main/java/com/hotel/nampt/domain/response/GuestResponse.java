package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Guest;

public class GuestResponse extends BaseResponse {
    private Guest guest;

    public Guest getGuest() {
        return guest;
    }

    public void setGuest(Guest guest) {
        this.guest = guest;
    }
}
