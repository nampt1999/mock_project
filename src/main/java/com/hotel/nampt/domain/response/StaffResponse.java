package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Staff;

public class StaffResponse extends BaseResponse {
    private Staff staff;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
