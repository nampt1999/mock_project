package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Bill;

public class BillResponse extends BaseResponse{
    private Bill bill;

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
