package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Booking;

import java.util.List;

public class BookingListResponse extends BaseResponse {
    private List<Booking> bookingList;

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }
}
