package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Guest;

import java.util.List;

public class GuestListResponse extends BaseResponse {
    private List<Guest> guestList;

    public List<Guest> getGuestList() {
        return guestList;
    }

    public void setGuestList(List<Guest> guestList) {
        this.guestList = guestList;
    }
}
