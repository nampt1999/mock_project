package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Staff;

import java.util.List;

public class StaffListResponse extends BaseResponse {
    List<Staff> staffList;

    public List<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(List<Staff> staffList) {
        this.staffList = staffList;
    }
}
