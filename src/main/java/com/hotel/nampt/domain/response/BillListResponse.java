package com.hotel.nampt.domain.response;

import com.hotel.nampt.model.Bill;

import java.util.List;

public class BillListResponse extends  BaseResponse {
    private List<Bill> billList;

    public List<Bill> getBillList() {
        return billList;
    }

    public void setBillList(List<Bill> billList) {
        this.billList = billList;
    }

}
