// load khi vào trang web luôn
$(document).ready(function () {
    // get all bill
    $.ajax({
        url: "http://localhost:8080/bill/billList",
        method: "GET"
    }).done(function (data) {
        console.log(data);
        var str = '';
        for (var i = 0; i < data.billList.length; i++) {
            str += "<tr>" +
                "<td>" + data.billList[i].billId + "</td>" +
                "<td>" + data.billList[i].bookingId + "</td>" +
                "<td>" + data.billList[i].paymentDate + "</td>" +
                "<td>" + data.billList[i].paymentAmount + "</td>" +
                "<td>" + data.billList[i].paymentType + "</td>" +
                "<td>" + data.billList[i].note + "</td>" +
                "<td>" + "<button class=\"btn btn-success mr-2 editBill\"  onclick=\"openNavToEdit();\"><i class=\"fas fa-pen-square\"></i></button>"
                + "<button class=\"btn btn-danger deleteBill\"  onclick=\"removeSelectedRow(this);\"><i class=\"fas fa-trash\"></i></button>" +
                "</td>" +
                "</tr>";
        }
        $('#table').find("tbody").append(str);
    });


    $.ajax({
        url: "http://localhost:8080/bookingList",
        method: "GET"
    }).done(function (data) {
        console.log(data);
        var str = '';
        for (var i = 0; i < data.bookingList.length; i++) {
            str += "<option value='" + data.bookingList[i].bookingId + "'>" + data.bookingList[i].bookingId + "</option>";
        }
        $('#booking_id').append(str);
    });
});

var rIndex, table = document.getElementById("table");

//add row bill
function addRow() {
    if (!checkEmptyInput()) {
        var bookingId = $('#booking_id').val(),
            paymentDate = $('#payment_date').val(),
            paymentAmount = $('#payment_amount').val(),
            note = $('#note').val(),
            paymentType = $("input[name='payment_type']:checked").val();
        console.log(paymentType);
        // thêm ở phía server
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/bill/save",
            contentType: 'application/json',
            data: JSON.stringify({
                bookingId: bookingId, paymentDate: paymentDate, paymentAmount: paymentAmount,
                paymentType: paymentType, note: note
            }), success: function (data) {
                console.log(data);
                // thêm ở phía client
                var newRow = table.insertRow(table.length),
                    cell0 = newRow.insertCell(0),
                    cell1 = newRow.insertCell(1),
                    cell2 = newRow.insertCell(2),
                    cell3 = newRow.insertCell(3),
                    cell4 = newRow.insertCell(4),
                    cell5 = newRow.insertCell(5),
                    cell6 = newRow.insertCell(6);

                cell0.innerHTML = data.bill.billId;
                cell1.innerHTML = data.bill.bookingId;
                cell2.innerHTML = data.bill.paymentDate;
                cell3.innerHTML = data.bill.paymentAmount;
                cell4.innerHTML = data.bill.paymentType;
                cell5.innerHTML = data.bill.note;
                cell6.innerHTML = " <button class='btn btn-success mr-2' onclick='openNavToEdit();'><i class='fas fa-pen-square'></i></button> " +
                    "<button class='btn btn-danger'  onclick='removeSelectedRow(this);'><i class='fas fa-trash'></i></button>";
                // call the function to set the event to the new row
                // selectedRowToInput();
                document.getElementById("myForm").style.display = "none";
            },
        });
    }
}

//delete row bill
function removeSelectedRow(button) {
    //lấy ra hàng
    row = button.parentElement.parentElement;
    // lấy chỉ mục dòng
    var rowNumber = row.rowIndex;
    // lấy giá trị trong cột 0:id
    var id = row.cells[0].innerHTML
    // xóa luôn ở phía client
    var table = document.getElementById("table");
    table.deleteRow(rowNumber);

    //và xóa luôn ở server
    if (confirm('Do you really want to delete record?')) {
        console.log(id);
        $.ajax({
            type: "DELETE",
            url: "http://localhost:8080/bill/delete/" + id,
            success: function (data) {
                // location.reload(true)
                console.log(data);
            },
            error: function () {
                $('#err').html('<span style=\'color:red; font-weight: bold; font-size: 30px;\'>Error deleting record').fadeIn().fadeOut(4000, function () {
                    $(this).remove();
                });
            }
        });
    }

}

//edit row bill

function editRow() {
    var billId = $('#bill_id').val(),
        bookingId = $('#booking_id').val(),
        paymentDate = $('#payment_date').val(),
        paymentAmount = $('#payment_amount').val(),
        note = $('#note').val(),
        paymentType = $("input[name='payment_type']:checked").val();
    table.rows[rIndex].cells[1].innerHTML = bookingId;
    table.rows[rIndex].cells[2].innerHTML = paymentDate;
    table.rows[rIndex].cells[3].innerHTML = paymentAmount;
    table.rows[rIndex].cells[4].innerHTML = paymentType;
    table.rows[rIndex].cells[5].innerHTML = note;
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/bill/save",
        contentType: 'application/json',
        data: JSON.stringify({
            billId: billId,
            bookingId: bookingId, paymentDate: paymentDate, paymentAmount: paymentAmount,
            paymentType: paymentType, note: note
        }), success: function (data) {
            console.log(data);
        },
    });
    document.getElementById("myForm").style.display = "none";
}

// display selected row data into input text
function selectedRowToInput() {

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            // get the seected row index
            rIndex = this.rowIndex;
            document.getElementById("bill_id").value = this.cells[0].innerHTML;
            document.getElementById("booking_id").value = this.cells[1].innerHTML;
            document.getElementById("payment_date").value = this.cells[2].innerHTML;
            document.getElementById("payment_amount").value = this.cells[3].innerHTML;
            if (this.cells[4].innerHTML === "card") {
                document.getElementById("card").checked = true;
            } else {
                document.getElementById("cash").checked = true;
            }
            document.getElementById("note").value = this.cells[5].innerHTML;
        };
    }
}

// clear data into form input text
function removedRowToInput() {
    document.getElementById("booking_id").value = "";
    document.getElementById("payment_date").value = "";
    document.getElementById("payment_amount").value = "";
    document.getElementById("card").checked = false;
    document.getElementById("cash").checked = false;
    document.getElementById("note").value = "";
}

// check the empty input
function checkEmptyInput() {
    var isEmpty = false, bookingId = document
        .getElementById("booking_id").value, paymentDate = document
            .getElementById("payment_date").value, paymentAmount = document
                .getElementById("payment_amount").value, card = document
                    .getElementById("card"), cash = document
                        .getElementById("cash"), note = document
                            .getElementById("note").value;
    if (bookingId === "") {
        alert("booking_id Name Cannot Be Empty");
        isEmpty = true;
    } else if (paymentDate === "") {
        alert("payment_date Cannot Be Empty");
        isEmpty = true;
    } else if (paymentAmount === "") {
        alert("payment_amount Cannot Be Empty");
        isEmpty = true;

    } else if ((card.checked == false) && (cash.checked == false)) {
        alert("paymentType cannot Be Empty");
        isEmpty = true;
    } else if (note === "") {
        alert("NOTE cannot Be Empty");
        isEmpty = true;
    }
    return isEmpty;
}
