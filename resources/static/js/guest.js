// load khi vào trang web luôn
$(document).ready(function () {
    // get all bill
    $.ajax({
        url: "http://localhost:8080/guestList",
        method: "GET"
    }).done(function (data) {
        console.log(data);
        var str = '';
        for (var i = 0; i < data.guestList.length; i++) {
            str += "<tr>" +
                "<td>" + data.guestList[i].staffId + "</td>" +
                "<td>" + data.guestList[i].staffName + "</td>" +
                "<td>" + data.guestList[i].address + "</td>" +
                "<td>" + data.guestList[i].phone + "</td>" +
                "<td>" + "<button class=\"btn btn-success mr-2 editBill\"  onclick=\" \"><i class=\"fas fa-pen-square\"></i></button>"
                + "<button class=\"btn btn-danger deleteBill\"  onclick=\" \"><i class=\"fas fa-trash\"></i></button>" +
                "</td>" +
                "</tr>";
        }
        $('#table').find("tbody").append(str);
    });

});