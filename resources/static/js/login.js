$(document).ready(function () {
    // check login
    $("#login").click(function () {
        var username = $('#username').val(),
            password = $('#password').val();
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/staffLogin",
            contentType: 'application/json',
            dataType: "json",
            data: JSON.stringify({
                username: username, password: password
            }),
            success: function (data) {
                console.log(data)
                if (data.code == 100) {
                    alert("đăng nhập thành công !");
                    document.cookie = `name=${data.staff.staffName}`;
                    location.assign("index.html");
                } else if (data.code == 101) {
                    alert("đăng nhập thất bại");

                }
            },
            error: function (error) {
                console.log(error);
                alert("error");
            }
        })
    });
});

